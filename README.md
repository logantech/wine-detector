# Classification: Gaussian Naive Bayes machine learning model

* Perform data cleaning, scaling, and exploratory data analysis on a dataset with multiple categorical and continuous features.
* Feature reduction through PCA, stepwise feature selection and correlation analysis.
* Build classification model using Gaussian Naive Bayes and evaluate performance.

Quickstart:

* Download Python 3 notebook: LoganDowning-L03-WineClassifier.ipynb
* Download datasource: RedWhiteWine.csv
* Open using Jupyter Notebook

Or view this [screen grab](LoganDowning-L03-WineClassifier.png) of the entire notebook.

___

Scale and Winsorize data to normalize to a common scale and handle extreme values.

![Distribution charts for six features](images/winsorising.png)

Examine correlation of each feature with outcome variable and with each other independent variable.

![Correlation matrix](images/corr.png)

Establishing the optimum number of features using accuracy scores based on KFold cross-validation of the training data.

![Bar chart showing effect of number of features on the accuracy score](images/feature-reduction.png)

Examine importance of each axis of the scaled, Winsorized data transformed into principal axes via PCA.

![Line chart showing the contributions of each PCA axis](images/pca.png)

Examine the separability of data for the first two PCA axes.

![Scatter plot showing the separability of the data once transformed via PCA](images/separability.png)
